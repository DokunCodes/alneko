<?php
include 'app/Crud.php';
$alneko = new Crud();
session_start();



if(isset($_GET['logout'])){
    session_destroy();
    header('location: ./');
}

$category = $alneko->fetchcategory();



?>

<!DOCTYPE html>

<head>

<!-- Basic Page Needs
================================================== -->
	<title>Alneko | Beauty Solutions</title>



<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/main-color.css" id="colors">
    <link href="css/app.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/alertify.css">

    <!-- Favicons-->
	<link rel="shortcut icon" href="images/fav.png" type="image/x-icon">
</head>

<body class="transparent-header">

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container">

	<!-- Header -->
	<div id="header">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="#"><img src="images/logo.png" data-sticky-logo="images/logo-bold.png" alt=""></a>
				</div>

				<!-- Mobile Navigation -->
				<div class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>


			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / End -->
            <?php if(isset($_SESSION['user_id'])){ ?>
			<div class="right-side">
				<div class="header-widget">
					<!-- User Menu -->
					<div class="user-menu">
						<div class="user-name"><span><img src="images/user.png" alt=""></span>Hi, <?php echo $_SESSION['user_name'] ?>!</div>
						<ul>
							<li><a href="bookings"><i class="fa fa-calendar-check-o"></i> Bookings</a></li>
                            <li><a href="dashboard-messages.html"><i class="sl sl-icon-envelope-open"></i> Transactions</a></li>
							<li><a href="?logout"><i class="sl sl-icon-power"></i> Logout</a></li>
						</ul>
					</div>


					<a href="vendor" class="button border with-icon">List Your Business <i class="sl sl-icon-plus"></i></a>
				</div>
			</div>
			<!-- Right Side Content / End -->
            <?php } else{ ?>
            <div class="right-side">
                <div class="header-widget">
                  <a href="#sign-in-dialog" class="button popup-with-zoom-anim"><i class="sl sl-icon-login"></i> Log In</a>

                    <a href="vendor" class="button border with-icon">List Your Business <i class="sl sl-icon-plus"></i></a>
                </div>
            </div>

             <?php } ?>

			<!-- Sign In Popup -->
			<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

				<div class="small-dialog-header">
					<h3>Sign In</h3>
				</div>

				<!--Tabs -->
				<div class="sign-in-form style-1">

					<ul class="tabs-nav">
						<li class=""><a href="#tab1">Log In</a></li>
						<li><a href="#tab2">Register</a></li>
					</ul>

					<div class="tabs-container alt">

						<!-- Login -->
						<div class="tab-content" id="tab1" style="display: none;">
							<form method="post" class="login">

								<p class="form-row form-row-wide">
									<label for="login_email">Username:
										<i class="im im-icon-Email"></i>
										<input type="text" class="input-text" required name="login_email" id="login_email" value="" />
									</label>
								</p>

								<p class="form-row form-row-wide">
									<label for="password">Password:
										<i class="im im-icon-Lock-2"></i>
										<input class="input-text" type="password" required name="login_password" id="password"/>
									</label>
									<span class="lost_password">
										<a href="#" >Lost Your Password?</a>
									</span>
								</p>

								<div class="form-row">
									<input type="submit" class="button border margin-top-5" name="login" value="Login" />
									<div class="checkboxes margin-top-10">
										<input id="remember-me" type="checkbox" name="check">
										<label for="remember-me">Remember Me</label>
									</div>
								</div>
								
							</form>
						</div>

						<!-- Register -->
						<div class="tab-content" id="tab2" style="display: none;">

							<form method="post" class="register">
								
							<p class="form-row form-row-wide">
								<label for="username2">Fullname:
									<i class="im im-icon-Male"></i>
									<input type="text" class="input-text" required name="fullname" id="username2" value="" />
								</label>
							</p>
								
							<p class="form-row form-row-wide">
								<label for="email2">Email Address:
									<i class="im im-icon-Mail"></i>
									<input type="email" class="input-text" required name="email" id="email2" value="" />
								</label>
							</p>

                                <p class="form-row form-row-wide">
                                    <label for="email2">Phone:
                                        <i class="im im-icon-Phone"></i>
                                        <input type="number" class="input-text" required name="phone" id="phone2" value="" />
                                    </label>
                                </p>


                                <p class="form-row form-row-wide">
								<label for="password1">Password:
									<i class="im im-icon-Lock-2"></i>
									<input class="input-text" type="password" required name="password" id="password1"/>
								</label>
							</p>


							<input type="submit" class="button border fw margin-top-10" name="register" value="Register" />
	
							</form>
						</div>

					</div>
				</div>
			</div>
			<!-- Sign In Popup / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->


<!-- Banner
================================================== -->
<div class="main-search-container centered" data-background-image="images/home_section_1.jpg">
	<div class="main-search-inner">

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>
						Find Nearby 
						<!-- Typed words can be configured in script settings at the bottom of this HTML file -->
						<span class="typed-words"></span>
					</h2>
					<h4>Discover African beauty vendors around you</h4>

                    <form action="listings" method="get">
                        <div class="main-search-input">


                            <div class="main-search-input-item location">
                                <div id="autocomplete-container">
                                    <input id="autocomplete-input" type="text" required placeholder="Enter Location / Postcode">
                                    <input type="hidden" id="location" name="location">
                                </div>
                                <a href="#"><i class="fa fa-map-marker"></i></a>
                            </div>



                            <div class="main-search-input-item">
                                <select data-placeholder="All Categories" name="category" required class="chosen-select" >
                                    <option value="">Select Category</option>
                                    <?php
                                    foreach ($category as $cat){
                                        echo '<option value="'.$cat->cat_id.'">'.$cat->cat_name.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <button class="button" type="submit" >Search</button>

                        </div>
                    </form>


				</div>
			</div>
			
			<!-- Features Categories -->
			<div class="row">
				<div class="col-md-12">
					<h5 class="highlighted-categories-headline">Or browse featured categories:</h5>

					<div class="highlighted-categories">

                        <?php
                        $j =0;
                        foreach ($category as $cat){
                            echo '
                            <a href="listings?category='.$cat->cat_id.'" class="highlighted-category">
					  
					    	<h4>'.$cat->cat_name.'</h4>
						</a>
                            ';

                            if($j==4) break;

                            $j++;



                        }
                        ?>

					</div>

				</div>
			</div>
			<!-- Featured Categories - End -->

		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline centered margin-top-75">
				<strong class="headline-with-separator">All Categories</strong>
			</h3>
		</div>

		<div class="col-md-12">
			<div class="categories-boxes-container margin-top-5 margin-bottom-30">

                <?php
                $j =0;
                foreach ($category as $cat){
                    echo '
                            <a href="listings?category='.$cat->cat_id.'" class="category-small-box" style="height: 200px">
					  
					    	<h4>'.$cat->cat_name.'</h4>
						</a>
                            ';




                }
                ?>


			</div>
		</div>
	</div>
</div>
<!-- Category Boxes / End -->






<!-- Info Section -->
<section class="fullwidth padding-top-75 padding-bottom-70">
<div class="container">

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3 class="headline centered headline-extra-spacing">
				<strong class="headline-with-separator">How it Works</strong>
				<span class="margin-top-25">
                    3 steps in finding a beauty vendor near you</span>
			</h3>
		</div>
	</div>

	<div class="row icons-container">
		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2 with-line">
				<i class="im im-icon-Map2"></i>
				<h3>Supply Location</h3>
				<p>Supply your nearest location, or turn on your location and search</p>
			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2 with-line">
				<i class="im im-icon-Information"></i>
				<h3>View Store Info</h3>
				<p>View search result based on selected criteria</p>
			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2">
				<i class="im im-icon-Checked-User"></i>
				<h3>Book, Reach or Call</h3>
				<p>Book an appointment with your preferred beauty vendor.</p>
			</div>
		</div>
	</div>

</div>
</section>
<!-- Info Section / End -->



<!-- Footer
================================================== -->
<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<img class="footer-logo" src="images/logo-bold.png" alt="">
				<br><br>
				<p>The Alneko is the UK’s first online beauty concierge dedicated to serving black girls!  We believe there is an alternative to spending hours scrolling through instagram, gumtree and twitter just trying to find a good beauty vendor.
                </p>
			</div>

			<div class="col-md-4 col-sm-6 ">
				<h4>Helpful Links</h4>
				<ul class="footer-links">
					<li><a href="#">Login</a></li>
					<li><a href="#">Sign Up</a></li>
					<li><a href="#">List Business</a></li>
					<li><a href="#">Pricing</a></li>

				</ul>

				<ul class="footer-links">
					<li><a href="#">FAQ</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
                    <li><a href="#">Privacy Policy</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>		

			<div class="col-md-3  col-sm-12">
				<h4>Contact Us</h4>
				<div class="text-widget">
					<span>12345 Little Lonsdale St, Melbourne</span> <br>
					Phone: <span>(123) 123-456 </span><br>
					E-Mail:<span> <a href="#"><span class="" data-cfemail=""></span></a> info@alneko.co.uk</span><br>
				</div>

				<ul class="social-icons margin-top-20">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="instagram" href="#"><i class="icon-instagram"></i></a></li>

				</ul>

			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">© 2019 Alneko UK. All Rights Reserved.</div>
			</div>
		</div>

	</div>

</div>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="scripts/mmenu.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/waypoints.min.js"></script>
<script type="text/javascript" src="scripts/counterup.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script src="scripts/alertify.js"></script>
<script src="scripts/app.min.js"></script>


<!-- Google Autocomplete -->
<script>



    function success(pos) {
        var crd = pos.coords;

        localStorage.setItem('lat',crd.latitude);
        localStorage.setItem('lon',crd.longitude);

    }

    function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    function loader(action){

        $.busyLoadFull(action, {
            spinner: "circles",
            color: "#fff",
            background: "rgba(0, 0, 0, 0.21)",
            maxSize: "50px",
            text: "Please wait...",
            fontSize: "3rem"

        });

    }


  function initAutocomplete() {



      var option = {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
      };


      if (navigator.geolocation) {

          navigator.geolocation.getCurrentPosition(success, error, option);
      }


    var input = document.getElementById('autocomplete-input');
      var options = {
          componentRestrictions: { country: "uk" }
      };
    var autocomplete = new google.maps.places.Autocomplete(input,options);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      else {
        var latx = autocomplete.getPlace().geometry.location.lat();
        var lonx = autocomplete.getPlace().geometry.location.lng();

        $('#location').val(latx+','+lonx);
      }
    });

	if ($('.main-search-input-item')[0]) {
	    setTimeout(function(){ 
	        $(".pac-container").prependTo("#autocomplete-container");
	    }, 300);
	}
}



    $('.register').submit(function (e) {

        e.preventDefault();

        loader('show');

        $.ajax({
            type:"POST",
            url:"app/auth.php",
            data:$(this).serialize(),
            success: function(response){

                loader('hide');

                let result = $.parseJSON(response);

                if(result[0]==='success'){

                    alertify.logPosition("top right");
                    alertify.success(result[1]);

                    location.reload();


                }
                else{

                    alertify.logPosition("top right");
                    alertify.error(result[1]);

                }


            }
        });






    });

    $('.login').submit(function (e) {

        e.preventDefault();

        loader('show');

        $.ajax({
            type:"POST",
            url:"app/auth.php",
            data:$(this).serialize(),
            success: function(response){

                loader('hide');



                let result = $.parseJSON(response);

                if(result[0]==='success'){

                    alertify.logPosition("top right");
                    alertify.success(result[1]);

                    location.reload();


                }
                else{

                    alertify.logPosition("top right");
                    alertify.error(result[1]);

                }


            }
        });






    });


</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcfsCPnwCnEBtedtZqbSb-dZtYronKHOI
&libraries=places&callback=initAutocomplete" async defer>  </script>


<!-- Typed Script -->
<script type="text/javascript" src="scripts/typed.js"></script>
<script>
    var typed = new Typed('.typed-words', {

        strings: [" Make Up Shops"," Hair Salon"," Nail Salon"," Teeth Whitening Shops", " Eyelashes & Gele Experts", " Tailors"],
        typeSpeed: 80,
        backSpeed: 80,
        backDelay: 4000,
        startDelay: 1000,
        loop: true,
        showCursor: true
    });
</script>



</body>


</html>