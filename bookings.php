<?php
include './app/Crud.php';
$alneko = new Crud();
session_start();

if(isset($_GET['logout'])){
    session_destroy();
    header('location: ./');
}

if(!isset($_SESSION['user_id'])){
    header('location: ./');
}

$user_id = $_SESSION['user_id'];
$biz_name = 'Tara';
//    $_SESSION['biz_name'];
$vendor_id = 1;
//    $_SESSION['vendor_id'];

$stats = $alneko->vendor_stats($vendor_id);

$bookings = $alneko->getBookings($user_id);

//var_dump($bookings);
//die();

?>


<!DOCTYPE html>

<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Alneko | Bookings </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/main-color.css" id="colors">

    <!-- Favicons-->
    <link rel="shortcut icon" href="./images/fav.png" type="image/x-icon">

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header Container
    ================================================== -->
    <header id="header-container" class="fixed fullwidth dashboard">

        <!-- Header -->
        <div id="header" class="not-sticky">
            <div class="container">

                <!-- Left Side Content -->
                <div class="left-side">

                    <!-- Logo -->
                    <div id="logo">
                        <a href="#"><img src="./images/logo-bold.png" alt=""></a>
                        <a href="#" class="dashboard-logo"><img src="./images/logo-bold.png" alt=""></a>
                    </div>

                    <!-- Mobile Navigation -->
                    <div class="mmenu-trigger">
                        <button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
                        </button>
                    </div>



                </div>
                <!-- Left Side Content / End -->

                <!-- Right Side Content / End -->
                <div class="right-side">
                    <!-- Header Widget -->
                    <div class="header-widget">

                        <!-- User Menu -->
                        <div class="user-menu">
                            <div class="user-name"><span><img src="./images/user.png" alt=""></span><?= $_SESSION['user_name']?></div>
                            <ul>

                                <li><a href="?logout"><i class="sl sl-icon-power"></i> Logout</a></li>
                            </ul>
                        </div>

                    </div>
                    <!-- Header Widget / End -->
                </div>
                <!-- Right Side Content / End -->

            </div>
        </div>
        <!-- Header / End -->

    </header>
    <div class="clearfix"></div>
    <!-- Header Container / End -->


    <!-- Dashboard -->
    <div id="dashboard">

        <!-- Navigation
        ================================================== -->
        <?php include './sidemenu.php'?>

        <!-- Content
   ================================================== -->
        <div class="dashboard-content">

            <!-- Titlebar -->
            <div id="titlebar">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Bookings</h2>
                        <!-- Breadcrumbs -->
                        <nav id="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li>Bookings</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="row">

                <!-- Listings -->
                <div class="col-lg-12 col-md-12">
                    <div class="dashboard-list-box margin-top-0">

                        <!-- Booking Requests Filters  -->
                        <div class="booking-requests-filter">

                            <!-- Date Range -->
                            <div id="booking-date-range">
                                <span></span>
                            </div>
                        </div>

                        <!-- Reply to review popup -->
                        <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                            <div class="small-dialog-header">
                                <h3>Send Message</h3>
                            </div>
                            <div class="message-reply margin-top-0">
                                <textarea cols="40" rows="3" placeholder="Your Message to Kathy"></textarea>
                                <button class="button">Send</button>
                            </div>
                        </div>

                        <h4>Booking Requests</h4>
                        <ul>

                            <?php

                            foreach ($bookings as $booking){
                                ?>

                            <li class="pending-booking">
                                <div class="list-box-listing bookings">
                                    <div class="list-box-listing-img">
                                        <?php
                                        if ($booking->photo == '' || empty($booking->photo)){
                                            ?>
                                            <img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=120" alt="">
                                        <?php
                                        }
                                        else{
                                            ?>
                                            <img src="<?=$booking->photo?>" alt="">
                                        <?php
                                        }
                                        ?>

                                    </div>
                                    <div class="list-box-listing-content">
                                        <div class="inner">
                                            <h3> <?= $booking->listing_name?>
                                                <?php
                                                if ($booking->status === 'pending'){
                                                    ?>
                                                    <span class="booking-status pending">Pending Approval</span>
                                                <?php
                                                }

                                                if ($booking->status === 'approved'){
                                                    ?>
                                                    <span class="booking-status pending">Pending Payment</span>
                                                    <?php
                                                }

                                                if ($booking->status === 'paid'){
                                                    ?>
                                                    <span class="booking-status paid">Paid</span>
                                                    <?php
                                                }
                                                ?>
                                            </h3>

                                            <div class="inner-booking-list">
                                                <h5>Booking Date:</h5>
                                                <ul class="booking-list">
                                                    <li class="highlighted"><?php
                                                        $bookingDate = new DateTime($booking->booking_date);
                                                        echo date_format( $bookingDate, 'g:ia \o\n l jS F Y')
                                                        ?></li>
                                                </ul>
                                            </div>

                                            <div class="inner-booking-list">
                                                <h5>Booking Details:</h5>
                                                <ul class="booking-list">
                                                    <?php
                                                    if ($booking->people == 1){
                                                        ?>
                                                        <li class="highlighted"><?= $booking->people?> Adult</li>
                                                    <?php
                                                    }

                                                    if ($booking->people > 1){
                                                        ?>
                                                        <li class="highlighted"><?= $booking->people?> Adults</li>
                                                        <?php
                                                    }
                                                    ?>

                                                </ul>
                                            </div>

                                            <div class="inner-booking-list">
                                                <h5>Price:</h5>
                                                <ul class="booking-list">
                                                    <li class="highlighted"><?= $booking->total_price?> £</li>
                                                </ul>
                                            </div>

                                            <div class="inner-booking-list">
                                                <h5>Services:</h5>
                                                <ul class="booking-list">
                                                    <li><?= $booking->total_services?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                if ($booking->status === 'approved'){
                                    ?>
                                    <div class="buttons-to-right" style="opacity: 1;">
                                        <a href="#" class="button approve purple"><i class="sl sl-icon-wallet"></i> Pay</a>
                                    </div>
                                <?php
                                }

                                if ($booking->status === 'payed'){
                                    ?>
<!--                                    <div class="buttons-to-right" style="opacity: 1;">-->
<!--                                        <a href="#" class="button approve purple"><i class="sl sl-icon-wallet"></i> Pay</a>-->
<!--                                    </div>-->
                                    <h5>Rate Listing</h5>
                                    <?php
                                }

                                ?>

                            </li>
                            <?php
                            }
                            ?>

<!--                            <li class="approved-booking">-->
<!--                                <div class="list-box-listing bookings">-->
<!--                                    <div class="list-box-listing-img"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=120" alt=""></div>-->
<!--                                    <div class="list-box-listing-content">-->
<!--                                        <div class="inner">-->
<!--                                            <h3>Burger House <span class="booking-status">Approved</span></h3>-->
<!---->
<!--                                            <div class="inner-booking-list">-->
<!--                                                <h5>Booking Date:</h5>-->
<!--                                                <ul class="booking-list">-->
<!--                                                    <li class="highlighted">10.12.2019 at 12:30 pm - 13:30 pm</li>-->
<!--                                                </ul>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="inner-booking-list">-->
<!--                                                <h5>Booking Details:</h5>-->
<!--                                                <ul class="booking-list">-->
<!--                                                    <li class="highlighted">2 Adults, 2 Children</li>-->
<!--                                                </ul>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="inner-booking-list">-->
<!--                                                <h5>Client:</h5>-->
<!--                                                <ul class="booking-list">-->
<!--                                                    <li>Kathy Brown</li>-->
<!--                                                    <li><a href="http://www.vasterad.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="1972786d7160597c61787469757c377a7674">[email&#160;protected]</a></li>-->
<!--                                                    <li>123-456-789</li>-->
<!--                                                </ul>-->
<!--                                            </div>-->
<!---->
<!--                                            <a href="#small-dialog" class="rate-review popup-with-zoom-anim"><i class="sl sl-icon-envelope-open"></i> Send Message</a>-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="buttons-to-right">-->
<!--                                    <a href="#" class="button gray reject"><i class="sl sl-icon-close"></i> Cancel</a>-->
<!--                                </div>-->
<!--                            </li>-->

                        </ul>
                    </div>
                </div>


                <!-- Copyrights -->
                <div class="col-md-12">
                    <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
                </div>
            </div>

        </div>
        <!-- Content / End -->


    </div>
    <!-- Dashboard / End -->


</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
<script type="text/javascript" src="./scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="./scripts/mmenu.min.js"></script>
<script type="text/javascript" src="./scripts/chosen.min.js"></script>
<script type="text/javascript" src="./scripts/slick.min.js"></script>
<script type="text/javascript" src="./scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="./scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="./scripts/waypoints.min.js"></script>
<script type="text/javascript" src="./scripts/counterup.min.js"></script>
<script type="text/javascript" src="./scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="./scripts/tooltips.min.js"></script>
<script type="text/javascript" src="./scripts/custom.js"></script>

<script src="./scripts/moment.min.js"></script>
<script src="./scripts/daterangepicker.js"></script>

<script>
    $(function() {


        var start = moment().subtract(29, 'days');
        var end = moment();



        function cb(start, end) {
            $('#booking-date-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            console.log($('#booking-date-range span').html(start.format('MMMM D, YYYY')));
        }
        cb(start, end);
        $('#booking-date-range').daterangepicker({
            "opens": "left",
            "autoUpdateInput": false,
            "alwaysShowCalendars": true,
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });

    // Calendar animation and visual settings
    $('#booking-date-range').on('show.daterangepicker', function(ev, picker) {
        $('.daterangepicker').addClass('calendar-visible calendar-animated bordered-style');
        $('.daterangepicker').removeClass('calendar-hidden');
    });
    $('#booking-date-range').on('hide.daterangepicker', function(ev, picker) {
        $('.daterangepicker').removeClass('calendar-visible');
        $('.daterangepicker').addClass('calendar-hidden');
    });
</script>


</body>

</html>