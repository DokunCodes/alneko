
<!-- Responsive Navigation Trigger -->
<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

<div class="dashboard-nav">
    <div class="dashboard-nav-inner">

        <ul data-submenu-title="Dashboard">
            <li><a href="bookings"><i class="fa fa-calendar-check-o"></i> Bookings</a></li>
            <li><a href="earnings"><i class="sl sl-icon-wallet"></i> Transactions</a></li>
        </ul>

    </div>
</div>
<!-- Navigation / End -->
