<?php

/**
 * Created by PhpStorm.
 * User: Aleadmin
 * Date: 7/24/2017
 * Time: 5:12 PM
 */


include_once 'Config.php';

class Crud extends Config
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getData($query)
    {
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $rows = array();

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }


    public function getError()
    {
        return $this->connection->error;
    }

    public function getLastID()
    {
        return $this->connection->insert_id;
    }


    public function execute($query)
    {
        $result = $this->connection->query($query);

        if ($result == false) {
            //echo 'Error: cannot execute the command';
            return false;
        } else {

            return $result;

        }
    }

    public function generateToken($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }



    public function delete($id, $table)
    {
        $query = "DELETE FROM $table WHERE id = $id";

        $result = $this->connection->query($query);

        if ($result == false) {
            echo 'Error: cannot delete id ' . $id . ' from table ' . $table;
            return false;
        } else {
            return true;
        }
    }

    public function newdelete($id,$col,$table)
    {
        $query = "DELETE FROM $table WHERE $col = '$id'";

        $result = $this->connection->query($query);

        if ($result == false) {
            echo 'Error: cannot delete id ' . $id . ' from table ' . $table;
            return false;
        } else {
            return true;
        }
    }

    public function check_email($email,$phone)
    {
        $query="SELECT user_id FROM users WHERE email='$email' OR phone = '$phone'";

        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) > 0) {
            return true;
        }

        return false;


    }

    public function login($email,$pass)
    {
        $query="SELECT fullname,email FROM users WHERE email='$email' AND password='$pass';";
        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) <= 0) {
            return false;
        }

        $name="";

        while ($row = $result->fetch_assoc()) {
            $name = $row['fullname'];

        }


        return $name;
    }

    public function vendor_login($email,$pass)
    {
        $query="SELECT * FROM vendor WHERE email='$email' AND password='$pass';";
        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) <= 0) {
            return false;
        }

        $res=[];

        while ($row = $result->fetch_assoc()) {

            $payload = new stdClass();

            $payload->ven_id = $row['vendor_id'];
            $payload->biz_name = $row['biz_name'];
            $payload->biz_info = $row['biz_info'];
            $payload->fname = $row['firstname'];
            $payload->lname = $row['lastname'];
            $payload->email = $row['email'];
            $payload->phone = $row['phone'];

            array_push($res,$payload);

        }


        return $res;
    }

    public function vendor_stats($vendor_id)
    {
        $query="SELECT * FROM analytics WHERE vendor_id=$vendor_id ";
        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) <= 0) {
            return false;
        }

        $res=[];

        while ($row = $result->fetch_assoc()) {

            $payload = new stdClass();

            $payload->services = $row['services'];
            $payload->views = $row['views'];
            $payload->reviews = $row['reviews'];
            $payload->bookings = $row['bookings'];


            array_push($res,$payload);

        }


        return $res;
    }


    public function getBookings($user_id){

        $query = "SELECT *, listings.status as listing_status, booking.status as booking_status, booking.created_at as booking_date FROM users  LEFT JOIN booking ON booking.user_id = users.user_id LEFT JOIN listings ON booking.listing_id = listings.list_id WHERE email = '$user_id'";
        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) <= 0) {
            return false;
        }

        $res=[];

        while ($row = $result->fetch_assoc()){

            $services = explode(',', $row['service']);

            $totalPrice = 0;
            $totalServices = '';
            foreach ($services as $service){
                $serviceInt = (int) $service;
                $serviceRow = $this->getService($serviceInt);
                $totalPrice = $totalPrice + $serviceRow['price'];
                $totalServices = $totalServices.', '.$serviceRow['title'];
            }
            $payload = new stdClass();

            $payload->listing_name = $row['name'];
            $payload->booking_date = $row['booking_date'];
            $payload->people = $row['people'];
            $payload->total_price = $totalPrice;
            $payload->total_services = $totalServices;
            $payload->status = $row['booking_status'];
            $payload->photo = $row['photo'];

            array_push($res,$payload);
        }

        return $res;
    }

    public function getService($service_id){

        $query = "SELECT * FROM services WHERE serv_id = $service_id";
        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) <= 0) {
            return false;
        }

        $row = $result->fetch_assoc();
        return $row;
    }


    public function vendor_booking($vendor_id)
    {
        $query="SELECT user_name(user_id) as user, b_id, date_accepted, status FROM booking WHERE vendor_id=$vendor_id AND date_accepted IS NOT NULL";
        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) <= 0) {
            return false;
        }

        $res=[];

        while ($row = $result->fetch_assoc()) {

            $payload = new stdClass();

            $payload->user = $row['user'];
            $payload->booking_id = $row['b_id'];
            $payload->date = date("F j, Y",strtotime($row['date_accepted']));;
            $payload->status = $row['status'];


            array_push($res,$payload);

        }


        return $res;
    }




    public function getvehicleplate($id)
    {
        $query="SELECT COUNT(id) as vhc FROM vehicle WHERE plate_number='$id'";
        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) <= 0) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['vhc'];
        }

        return $name;
    }

    public function totalstockleft($id)
    {
        $query="SELECT total_unit FROM warehouse WHERE stock_id='$id';";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['total_unit'];
        }

        return $name;
    }

    public function fetchcategory()
    {
        $query="SELECT category_id, category_name FROM category";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $res = [];

        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();

            $payload->cat_id = $row['category_id'];
            $payload->cat_name = $row['category_name'];


            array_push($res,$payload);
        }




        return $res;
    }

    public function totalavailable($id)
    {
        $query="SELECT total_unit FROM warehouse WHERE stock_id='$id';";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['total_unit'];
        }

        return $name;
    }

    public function getpartsleft($id)
    {
        $query="SELECT serial FROM parts WHERE part_id='$id';";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $serial = '';

        while ($row = $result->fetch_assoc()) {
            $serial = $row['serial'];
        }

        return $serial;
    }

    public function checkdata($id,$rows,$table)
    {
        $query = "SELECT $rows FROM $table WHERE id = $id";

        $result = $this->connection->query($query);

        if ($result == false) {
            echo 'Error: cannot delete id ' . $id . ' from table ' . $table;
            return false;
        } else {
            return true;
        }
    }


    public function escape_string($value)
    {
        return $this->connection->real_escape_string(trim(preg_replace('/\t+/', '',$value)));
    }

    public function sendsms($recipient,$message)
    {

        $url = "https://jusibe.com/smsapi/send_sms";
        $username = "4583587807b11a4b865f2e49a1cc00c1";
        $password = "bb3de67959fe497d291f8cea5b58f8bb";

        $data = array(
            "to" => $recipient,
            "from" => "CAAP LTD",
            "message" => $message,
        );


        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        curl_close($ch);

        $resp = json_decode($response, true);
        $msgid = $resp['message_id'];
        $stat = $resp['status'];
        $credit = $resp['sms_credits_used'];




        $this->execute("INSERT INTO smslog VALUES (NULL ,'$recipient','$message','$stat','$msgid','$credit',now())");


    }

    public static function sendmail($to,$txt,$subj)
    {


        $headers = "From: ALNEKO" . "\r\n";

        mail($to,$subj,$txt,$headers);


    }

    public function getToken($length=7){
        $token = "";
        $codeAlphabet= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-3)];
        }

        return $token;
    }

    public function getdrivervehicle($did)
    {
        $name='';
        $query="SELECT vehicle_make,vehicle_model,plate_number FROM vehicle WHERE driver_id='$did'";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name .= '- '.$row['vehicle_make']." ".$row['vehicle_model']." (".$row['plate_number'].")<br> ";
        }

        return $name;
    }

    public function getworkshop($did)
    {
        $query="SELECT workshop_name FROM workshop WHERE workshop_id='$did'";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['workshop_name'];
        }

        return $name;
    }

    public function getClientDetails($cid)
    {
        $query="SELECT client_name,client_email,client_phone FROM clients WHERE client_id='$cid'";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }


        while ($row = $result->fetch_assoc()) {
            $name = array($row['client_name'],$row['client_email'],$row['client_phone']);
        }

        return @$name;
    }


}