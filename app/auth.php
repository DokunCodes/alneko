<?php
/**
 * Created by PhpStorm.
 * User: seyiadedokun
 * Date: 2019-07-20
 * Time: 17:06
 */

include_once("Crud.php");
$user = new Crud();

if(isset($_POST['fullname'])){


    $fullname = $user->escape_string($_POST['fullname']);

    $user_email = $user->escape_string($_POST['email']);

    $user_phone = $user->escape_string($_POST['phone']);

    $user_password = sha1(md5($user->escape_string($_POST['password'])));


    if($user->check_email($user_email,$user_phone)){

        echo json_encode(array("error","Email or Phone already exist"));

    }
    else if($user->execute("INSERT INTO users VALUES (NULL,'$fullname','$user_email','$user_password','$user_phone','',now())")!=false) {

            echo json_encode(array("success","Registration Successful",$fullname));

            session_start();

            $_SESSION['user_id'] = $user_email;
            $_SESSION['user_name'] = explode(" ",$fullname)[0];



    }
    else{

        echo json_encode(array("error","An error occurred, please try again"));

    }


}
else if(isset($_POST['login_email'])){

    $email = $user->escape_string($_POST['login_email']);

    $pass = sha1(md5($user->escape_string($_POST['login_password'])));

    $result = $user->login($email,$pass);

    if(!$result){
        echo json_encode(array("error","Invalid Username/Password"));
    }
    else{
        session_start();

        $_SESSION = array();

        $_SESSION['user_id'] = $email;
        $_SESSION['user_name'] = explode(" ",$result)[0];

        echo json_encode(array("success","Login Successful"));

    }

}

else if(isset($_POST['vendor_login_email'])){

    $email = $user->escape_string($_POST['vendor_login_email']);

    $pass = sha1(md5($user->escape_string($_POST['vendor_login_password'])));

    $result = $user->vendor_login($email,$pass);

    if(!$result){
        echo json_encode(array("error","Invalid Username/Password"));
    }
    else{


        session_start();
        $_SESSION = array();

        $_SESSION['vendor_id'] = $result[0]->ven_id;
        $_SESSION['biz_name'] = $result[0]->biz_name;
        $_SESSION['biz_info'] = $result[0]->biz_info;
        $_SESSION['fname'] = $result[0]->fname;
        $_SESSION['lname'] = $result[0]->lname;
        $_SESSION['email'] = $result[0]->email;
        $_SESSION['phone'] = $result[0]->phone;

        echo json_encode(array("success","Login Successful"));

    }

}