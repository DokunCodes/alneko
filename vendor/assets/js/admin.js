/// Jquery validate contact form detail page
$('#login').submit(function () {

    var action = 'scripts/auth.php';

    $('#sbm').hide();
    $('#loader').show();

        $.post(action, {
                username: $('#username').val(),
                password: $('#password').val(),

            },
            function (data) {


                $('#loader').hide();
                $('#sbm').show();

                let result = $.parseJSON(data);

                if(result[0]==='success'){
                    window.location = "dashboard";
                }
                else{
                    alertify.logPosition("top right");
                    alertify.error(result[1]);

                }


    });
    return false;
});
