$(function() {





    function showPosition() {

        let latx,lonx;

        let input = document.getElementById("rest_addr");
        let lat = document.getElementById("lat");
        let lng = document.getElementById("lng");





        //Autocomplete Section
        let options = {
            componentRestrictions: { country: "ng" }
        };
        autocomplete = new google.maps.places.Autocomplete(input, options);

        google.maps.event.addDomListener(input, 'keydown', function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
            }
        });


        autocomplete.setFields(["address_components", "geometry", "icon", "name"]);

        autocomplete.addListener("place_changed", function () {

            var place = autocomplete.getPlace();

            if (!place.geometry) {
                alert(
                    "Location not found. Please type closest street/bus stop"
                );
                return;
            }
            else{
                latx = autocomplete.getPlace().geometry.location.lat();
                lonx = autocomplete.getPlace().geometry.location.lng();
                lat.value = latx;
                lng.value = lonx;





            }
        });
    }

    showPosition();

});