
$(document).ready(function() {


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {

                $('#rest_logo_prev').attr('src', e.target.result);

                $('#rest_logo_prev').show();

            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#customFile").change(function() {
        readURL(this);
    });



// S A V E - R E S T A U R A N T


    $("#add_restaurant").on('submit', function(e){
        e.preventDefault();

        var action = 'scripts/auth.php';
        var form = new FormData();

        form.append( 'logo', $('#customFile')[0].files[0]);
        form.append('restaurant_name', $('#rest_name').val());
        form.append('restaurant_addr', $('#rest_addr').val());
        form.append('restaurant_lng', $('#lng').val());
        form.append('restaurant_lat', $('#lat').val());

        if(!$('#lng').val()){
            alertify.logPosition("top right");
            alertify.error('Invalid address selected');
        }
        else{
            $('#sbm').hide();
            $('#loader').show();

            $.ajax({
                url: action,
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    $('#loader').hide();
                    $('#sbm').show();



                    let result = $.parseJSON(data);

                    if(result[0]==='success'){

                        $('#rest_logo_prev').hide();
                        $('#rest_name').val('');
                        $('#rest_addr').val('');


                        alertify.logPosition("top right");
                        alertify.success('Restaurant Added');

                    }
                    else if(result[1]){
                        alertify.logPosition("top right");
                        alertify.error(result[1]);

                    }
                    else{
                        alertify.logPosition("top right");
                        alertify.error('An error occurred try again later');

                    }


                }
            });

        }




    });


// U P D A T E - R E S T A U R A N T


    $("#update_restaurant").on('submit', function(e){
        e.preventDefault();

        var action = 'scripts/auth.php';
        var form = new FormData();

        form.append( 'logo', $('#customFile')[0].files[0]);
        form.append('up_restaurant_name', $('#rest_name').val());
        form.append('up_restaurant_addr', $('#rest_addr').val());
        form.append('up_restaurant_lng', $('#lng').val());
        form.append('up_restaurant_lat', $('#lat').val());
        form.append('up_rest_id', $('#restaurant_id').val());

        if(!$('#lng').val()){
            alertify.logPosition("top right");
            alertify.error('Invalid address supplied');
        }
        else{
            $('#sbm').hide();
            $('#loader').show();

            $.ajax({
                url: action,
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    $('#loader').hide();
                    $('#sbm').show();



                    let result = $.parseJSON(data);

                    if(result[0]==='success'){


                        alertify.logPosition("top right");
                        alertify.success('Restaurant Updated');

                    }
                    else{
                        alertify.logPosition("top right");
                        alertify.error('An error occurred try again later');

                    }


                }
            });

        }




    });








});






