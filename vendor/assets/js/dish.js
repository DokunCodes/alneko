
$(document).ready(function() {

    var cleaveG = new Cleave('#inputNumeral', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });



    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {

                $('#rest_logo_prev').attr('src', e.target.result);

                $('#rest_logo_prev').show();

            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#customFile").change(function() {
        readURL(this);
    });



// S A V E - D I S H


    $("#add_dish").on('submit', function(e){
        e.preventDefault();

        var action = 'scripts/auth.php';
        var form = new FormData();

        form.append( 'dish_photo', $('#customFile')[0].files[0]);
        form.append('dish_name', $('#dish_name').val());
        form.append('dish_price', $('#inputNumeral').val());
        form.append('restaurant_id', $('#rest_id').val());

            $('#sbm').hide();
            $('#loader').show();

            $.ajax({
                url: action,
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    $('#loader').hide();
                    $('#sbm').show();



                    let result = $.parseJSON(data);

                    if(result[0]==='success'){

                        $('#rest_logo_prev').hide();
                        $('#dish_name').val('');
                        $('#inputNumeral').val('');
                        $('#rest_id').val('');


                        alertify.logPosition("top right");
                        alertify.success('Dish Added');

                    }
                    else if(result[1]){
                        alertify.logPosition("top right");
                        alertify.error(result[1]);

                    }
                    else{
                        alertify.logPosition("top right");
                        alertify.error('An error occurred try again later');

                    }


                }
            });





    });


// U P D A T E - R E S T A U R A N T


    $("#update_dish").on('submit', function(e){
        e.preventDefault();

        var action = 'scripts/auth.php';
        var form = new FormData();

        form.append( 'up_dish_photo', $('#customFile')[0].files[0]);
        form.append('up_dish_name', $('#dish_name').val());
        form.append('up_dish_id', $('#dish_id').val());
        form.append('up_dish_price', $('#inputNumeral').val());
        form.append('up_restaurant_id', $('#rest_id').val());

            $('#sbm').hide();
            $('#loader').show();

            $.ajax({
                url: action,
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    $('#loader').hide();
                    $('#sbm').show();



                    let result = $.parseJSON(data);

                    if(result[0]==='success'){


                        alertify.logPosition("top right");
                        alertify.success('Dish Updated');

                    }
                    else if(result[1]){
                        alertify.logPosition("top right");
                        alertify.error(result[1]);

                    }
                    else{
                        alertify.logPosition("top right");
                        alertify.error('An error occurred try again later');

                    }


                }
            });



    });








});






