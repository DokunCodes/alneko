<?php
session_start();


if(isset($_SESSION['logout'])){
    session_destroy();
    header('location: ./');
}

?>


<!DOCTYPE html>


<head>

    <title>Alneko | Vendor Login </title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/main-color.css" id="colors">
    <link href="../css/app.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/alertify.css">

    <!-- Favicons-->
    <link rel="shortcut icon" href="../images/fav.png" type="image/x-icon">


</head>

<body>

<!-- Wrapper -->
<div id="wrapper">



    <!-- Content
    ================================================== -->

    <div class="container">

        <div class="row">
            <!-- Sign In Popup -->
            <div id="sign-in-dialog" class="zoom-anim-dialog">


                <!--Tabs -->
                <div class="sign-in-form style-1">
                    <div class="text-center" style="display: block !important; margin-bottom: 30px">
                        <a href="./"><img style="max-width: 250px" src="../images/logo-bold.png" alt=""></a>
                    </div>


                    <h4 class="text-center" style="color: #e77817 ">Welcome Back Vendor</h4>
                    <h6 class="text-center">Supply your email and password to accept bookings</h6>

                    <div class="tabs-container alt">

                        <!-- Login -->
                        <div class="tab-content" id="tab1" style="">
                            <form method="post" class="login" id="login">

                                <p class="form-row form-row-wide">
                                    <label for="username">Email:
                                        <i class="im im-icon-Email"></i>
                                        <input type="email" class="input-text" required name="vendor_login_email" id="username" value="" />
                                    </label>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="password">Password:
                                        <i class="im im-icon-Lock-2"></i>
                                        <input class="input-text" type="password" required name="vendor_login_password" id="password"/>
                                    </label>
                                    <span class="lost_password">
										<a href="#" >Lost Your Password?</a>
									</span>
                                </p>

                                <div class="form-row form-row-wide text-center">
                                    <input type="submit" class="button border margin-top-5" name="login" value="Login" />

                                </div>

                            </form>

                            <p style="margin-top: 20px; font-size: 12px; text-align: center"><a href="register"> Don't have an account yet? Sign up here</a></p>
                        </div>



                    </div>
                </div>
            </div>
            <!-- Sign In Popup / End -->
        </div>

    </div>



</div>
<!-- Wrapper / End -->



<!-- Scripts
================================================== -->

<script type="text/javascript" src="../scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="../scripts/mmenu.min.js"></script>
<script type="text/javascript" src="../scripts/chosen.min.js"></script>
<script type="text/javascript" src="../scripts/jquery-ui.min.js"></script>
<script src="../scripts/alertify.js"></script>
<script src="../scripts/app.min.js"></script>
<script>

    function loader(action){

        $.busyLoadFull(action, {
            spinner: "circles",
            color: "#fff",
            background: "rgba(0, 0, 0, 0.21)",
            maxSize: "50px",
            text: "Please wait...",
            fontSize: "3rem"

        });

    }

    $('.login').submit(function (e) {

        e.preventDefault();

        loader('show');

        $.ajax({
            type:"POST",
            url:"../app/auth.php",
            data:$(this).serialize(),
            success: function(response){

                loader('hide');

                console.log(response);


                let result = $.parseJSON(response);

                if(result[0]==='success'){

                    alertify.logPosition("top right");
                    alertify.success(result[1]+". Loading your dashboard...");

                    setInterval(()=>{
                        window.location.href="dashboard";
                    }, 300)



                }
                else{

                    alertify.logPosition("top right");
                    alertify.error(result[1]);

                }


            }
        });






    });
</script>





</body>

</html>