<?php
include_once("scripts/Crud.php");
session_start();
$pending_orders = $_SESSION['pending_orders'];

if(!$_SESSION['admin_name']){

    header('location: ./');
}
$admin = new Crud();




?>

<!DOCTYPE html>
<html lang="en">


<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Meta -->
    <meta name="description" content="BoiBoi Admin">
    <meta name="author" content="Seyi Adedokun">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <title>All Orders</title>

    <!-- vendor css -->
    <link href="lib/%40fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/jqvmap/jqvmap.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="assets/css/dashforge.css">
    <link rel="stylesheet" href="assets/css/alertify.css">
    <link href="lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="lib/select2/css/select2.min.css" rel="stylesheet">
</head>
<body>

<aside class="aside aside-fixed">
    <div class="aside-header">
        <a href="dashboard" class="aside-logo">boiboi<span>&nbsp;admin</span></a>
        <a href="#" class="aside-menu-link">
            <i data-feather="menu"></i>
            <i data-feather="x"></i>
        </a>
    </div>
    <div class="aside-body">
        <div class="aside-loggedin">
            <div class="d-flex align-items-center justify-content-start">
                <a href="#" class="avatar"><img src="assets/img/user.png" class="rounded-circle" alt=""></a>
                <div class="aside-alert-link">

                    <a href="dashboard?logout" data-toggle="tooltip" title="Sign out"><i data-feather="log-out"></i></a>
                </div>
            </div>
            <div class="aside-loggedin-user">
                <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
                    <h6 class="tx-semibold mg-b-0">Ahmad</h6>
                    <i data-feather="chevron-down"></i>
                </a>
                <p class="tx-color-03 tx-12 mg-b-0">Administrator</p>
            </div>
            <div class="collapse" id="loggedinMenu">
                <ul class="nav nav-aside mg-b-0">
                    <li class="nav-item"><a href="dashboard?logout" class="nav-link"><i data-feather="log-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
        </div><!-- aside-loggedin -->
        <ul class="nav nav-aside">
            <li class="nav-label">Dashboard</li>

            <li class="nav-item"><a href="dashboard" class="nav-link"><i data-feather="globe"></i> <span>Analytics</span></a></li>


            <li class="nav-item with-sub">
                <a href="#" class="nav-link"><i data-feather="target"></i> <span>Restaurants</span></a>
                <ul>
                    <li><a href="add-restaurant">Add New</a></li>
                    <li><a href="all-restaurant">View Restaurants</a></li>
                </ul>
            </li>
            <li class="nav-item with-sub">
                <a href="#" class="nav-link"><i data-feather="package"></i> <span>Dishes</span></a>
                <ul>
                    <li><a href="add-dish">Add New</a></li>
                    <li><a href="all-dish">View Dishes</a></li>
                </ul>
            </li>

            <li class="nav-item"><a href="customers" class="nav-link"><i data-feather="users"></i> <span>Customers</span></a></li>

            <?php

            if($pending_orders > 0){
                ?>
                <li class="nav-item active"><a href="orders" class="nav-link"><i data-feather="shopping-cart"></i>
                        <span>Orders &nbsp;&nbsp;<span class="badge badge-pill badge-primary"><?php echo $pending_orders?></span></span></a></li>

            <?php } else{?>
                <li class="nav-item active"><a href="orders" class="nav-link"><i data-feather="shopping-cart"></i> <span>Orders</span></a></li>

            <?php } ?>

        </ul>
    </div>
</aside>

<div class="content ht-100v pd-0">
    <div class="content-header">
        &nbsp;
    </div>
    <!-- content-header -->

    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                            <li class="breadcrumb-item"><a href="#">Orders</a></li>

                        </ol>
                    </nav>

                    <h4 class="mg-b-0 tx-spacing--1">All Orders</h4>


                </div>

            </div>

            <div class="row row-xs">



                <div class="col-lg-12 col-xl-12 mg-t-10">

                    <table id="example1" class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th class="wd-20p">Dish Name</th>
                            <th class="wd-10p">Quantity</th>
                            <th class="wd-15p">Customer</th>
                            <th class="wd-10p">Mode</th>
                            <th class="wd-10p">Amount</th>
                            <th class="wd-10p">Status</th>
                            <th class="wd-15p">Order Date</th>
                            <th class="wd-20p">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $orders = $admin->all_orders();



                        for ($j=0; $j<count($orders); $j++){

                          ?>
                            <tr id="order<?php echo $orders[$j]->order_id  ?>">
                                <td><?php echo $j+1 ?></td>
                                <td><?php echo $orders[$j]->d_name ?></td>
                                <td><?php echo $orders[$j]->qty ?></td>
                                <td><?php echo $orders[$j]->c_name ?></td>
                                <td><?php echo $orders[$j]->mode ?></td>
                                <td><?php echo $orders[$j]->amount  ?></td>
                                <td id="ostat<?php echo $orders[$j]->order_id  ?>"><?php echo $orders[$j]->status  ?></td>
                                <td><?php echo $orders[$j]->o_date  ?></td>
                                <td>
                                    <a href="#" onclick="cancelOrder('<?php echo $orders[$j]->order_id  ?>')" data-toggle="tooltip"  title="Cancel Order"> <i style="color: orangered" class="fas fa-trash-restore"></i></a>  &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="#" onclick="markCompleted('<?php echo $orders[$j]->order_id  ?>')" data-toggle="tooltip"  title="Mark Completed"> <i style="color: green" class="fas fa-check-circle"></i></a>  &nbsp;&nbsp;&nbsp;&nbsp;

                                    <a href="#" onclick="deleteOrder('<?php echo $orders[$j]->order_id  ?>')" data-toggle="tooltip"  title="Delete Order"> <i style="color: red" class="fas fa-trash"></i></a>
                                </td>
                            </tr>

                        <?php
                        }

                        ?>




                        </tbody>
                    </table>



                </div><!-- col -->



            </div><!-- row -->


        </div><!-- container -->
    </div>
</div>

<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="lib/feather-icons/feather.min.js"></script>
<script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="assets/js/dashforge.js"></script>
<script src="assets/js/dashforge.aside.js"></script>
<script src="lib/parsleyjs/parsley.min.js"></script>
<script src="assets/js/alertify.js"></script>
<script src="lib/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
<script src="lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>

<script src="lib/select2/js/select2.min.js"></script>
<script>
    // A L L - R E S T A U R A N T

    $('#example1').DataTable({
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copy',
                text: 'Copy to Clipboard',
                className: 'btn btn-primary wd-200',
                exportOptions: {
                    columns: 'th:not(:last-child)'
                }
            },
            {
                extend: 'excel',
                text: 'Export as Excel',
                className: 'btn btn-success wd-200',
                exportOptions: {
                    columns: 'th:not(:last-child)'
                }
            },
            {
                extend: 'pdf',
                text: 'Export as PDF',
                className: 'btn btn-danger wd-200',
                exportOptions: {
                    columns: 'th:not(:last-child)'
                }
            }
        ]
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    function deleteOrder(id) {


        alertify.confirm("Delete Order?",
            function(){

                var action = 'scripts/auth.php';

                $.post(action, {
                        delete_order: id

                    },
                    function (data) {


                        let result = $.parseJSON(data);

                        if(result[0]){
                            alertify.logPosition("top right");
                            alertify.success('Order Deleted');
                            $('#order'+id).remove();
                        }
                        else{
                            alertify.logPosition("top right");
                            alertify.error("An error occurred while deleting. Please try again");

                        }


                    });


            });
    }

    function markCompleted(id) {


        alertify.confirm("Mark Order Completed?",
            function(){

                var action = 'scripts/auth.php';

                $.post(action, {
                        complete_order: id

                    },
                    function (data) {


                        let result = $.parseJSON(data);

                        if(result[0]){
                            alertify.logPosition("top right");
                            alertify.success('Order marked completed');
                            $('#ostat'+id).html('Completed');
                        }
                        else{
                            alertify.logPosition("top right");
                            alertify.error("An error occurred while deleting. Please try again");

                        }


                    });


            });
    }

    function cancelOrder(id) {


        alertify.confirm("Cancel Order?",
            function(){

                var action = 'scripts/auth.php';

                $.post(action, {
                        cancel_order: id

                    },
                    function (data) {


                        let result = $.parseJSON(data);

                        if(result[0]){
                            alertify.logPosition("top right");
                            alertify.success('Order canceled');
                            $('#ostat'+id).html('Canceled');
                        }
                        else{
                            alertify.logPosition("top right");
                            alertify.error("An error occurred while deleting. Please try again");

                        }


                    });


            });
    }



</script>


</body>


</html>
