<?php
include_once("scripts/Crud.php");
session_start();
if(!$_SESSION['admin_name']){

    header('location: ./');
}

$pending_orders = $_SESSION['pending_orders'];

$admin = new Crud();

$dish_id = $_GET['id'];

$dish = $admin->get_dish_by_id($dish_id);

$restaurants = $admin->all_restaurants();



?>

<!DOCTYPE html>
<html lang="en">


<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Meta -->
    <meta name="description" content="BoiBoi Admin">
    <meta name="author" content="Seyi Adedokun">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <title>Update Restaurant</title>

    <!-- vendor css -->
    <link href="lib/%40fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/jqvmap/jqvmap.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="assets/css/dashforge.css">
    <link rel="stylesheet" href="assets/css/dashforge.dashboard.css">
    <link rel="stylesheet" href="assets/css/alertify.css">
</head>
<body>

<aside class="aside aside-fixed">
    <div class="aside-header">
        <a href="dashboard" class="aside-logo">boiboi<span>&nbsp;admin</span></a>
        <a href="#" class="aside-menu-link">
            <i data-feather="menu"></i>
            <i data-feather="x"></i>
        </a>
    </div>
    <div class="aside-body">
        <div class="aside-loggedin">
            <div class="d-flex align-items-center justify-content-start">
                <a href="#" class="avatar"><img src="assets/img/user.png" class="rounded-circle" alt=""></a>
                <div class="aside-alert-link">

                    <a href="dashboard?logout" data-toggle="tooltip" title="Sign out"><i data-feather="log-out"></i></a>
                </div>
            </div>
            <div class="aside-loggedin-user">
                <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
                    <h6 class="tx-semibold mg-b-0">Ahmad</h6>
                    <i data-feather="chevron-down"></i>
                </a>
                <p class="tx-color-03 tx-12 mg-b-0">Administrator</p>
            </div>
            <div class="collapse" id="loggedinMenu">
                <ul class="nav nav-aside mg-b-0">
                    <li class="nav-item"><a href="dashboard?logout" class="nav-link"><i data-feather="log-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
        </div><!-- aside-loggedin -->
        <ul class="nav nav-aside">
            <li class="nav-label">Dashboard</li>

            <li class="nav-item"><a href="dashboard" class="nav-link"><i data-feather="globe"></i> <span>Analytics</span></a></li>


            <li class="nav-item with-sub">
                <a href="#" class="nav-link"><i data-feather="target"></i> <span>Restaurants</span></a>
                <ul>
                    <li><a href="add-restaurant">Add New</a></li>
                    <li><a href="all-restaurant">View Restaurants</a></li>
                </ul>
            </li>
            <li class="nav-item show active with-sub">
                <a href="#" class="nav-link"><i data-feather="package"></i> <span>Dishes</span></a>
                <ul>
                    <li><a href="add-dish">Add New</a></li>
                    <li><a href="all-dish" class="active">View Dishes</a></li>
                </ul>
            </li>

            <li class="nav-item"><a href="customers" class="nav-link"><i data-feather="users"></i> <span>Customers</span></a></li>

            <?php

            if($pending_orders > 0){
                ?>
                <li class="nav-item"><a href="orders" class="nav-link"><i data-feather="shopping-cart"></i>
                        <span>Orders &nbsp;&nbsp;<span class="badge badge-pill badge-primary"><?php echo $pending_orders?></span></span></a></li>

            <?php } else{?>
                <li class="nav-item"><a href="orders" class="nav-link"><i data-feather="shopping-cart"></i> <span>Orders</span></a></li>

            <?php } ?>

        </ul>
    </div>
</aside>

<div class="content ht-100v pd-0">
    <div class="content-header">
        &nbsp;
    </div>
    <!-- content-header -->

    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                            <li class="breadcrumb-item"><a href="#">Dish</a></li>

                        </ol>
                    </nav>

                        <h4 class="mg-b-0 tx-spacing--1">Update Dish</h4>


                </div>

            </div>

            <div class="row row-xs">

                <div class="col-lg-2 col-xl-2 mg-t-10">
                    &nbsp;
                </div>

                <div class="col-lg-8 col-xl-8 mg-t-10">


                    <form action=""  method="post" name="update_dish" id="update_dish" data-parsley-validate="" enctype="multipart/form-data" >
                        <div class="wd-500">
                            <div class="d-md-flex mg-b-30">
                                <div class="form-group mg-b-0">
                                    <label>Dish Name: <span class="tx-danger">*</span></label>
                                    <input type="hidden" name="dish_id" id="dish_id" value="<?php echo $dish[0]->dish_id?>">
                                    <input type="text" name="dish_name" id="dish_name" value="<?php echo $dish[0]->dish_name?>"  class="form-control wd-400" placeholder="Dish name" required>
                                </div><!-- form-group -->

                            </div>

                            <div class="d-md-flex mg-b-30">
                                <div class="form-group mg-b-0">
                                    <label>Dish Price (₦): <span class="tx-danger">*</span></label>
                                    <input id="inputNumeral" type="text" class="form-control wd-400" value="<?php echo $dish[0]->price?>" required placeholder="Cost Per Meal">

                                </div><!-- form-group -->

                            </div>

                            <div class="d-md-flex mg-b-30">
                                <div class="form-group mg-b-0">
                                    <label>Restaurant: <span class="tx-danger">*</span></label>
                                    <select class="form-control wd-400" id="rest_id" required>
                                        <option></option>
                                        <?php

                                        for ($j=0; $j<count($restaurants); $j++){

                                           $select = $dish[0]->rest_id == $restaurants[$j]->rest_id ? 'selected' : '';

                                            echo "<option ".$select."  value='".$restaurants[$j]->rest_id."'>".$restaurants[$j]->name."</option>";

                                        }

                                        ?>
                                    </select>
                                </div><!-- form-group -->

                            </div><!-- d-flex -->



                            <div class="d-md-flex mg-b-30">

                                <img id="rest_logo_prev" src="<?php echo $dish[0]->image ?>" style="max-width: 200px" class="img-thumbnail" alt="Responsive image">

                            </div>


                            <div class="d-md-flex mg-b-30">

                                <div class="form-group mg-b-0">
                                    <div class="custom-file">
                                        <input type="file" name="dish_photo" accept="image/*" class="custom-file-input wd-400" id="customFile">
                                        <label class="custom-file-label" for="customFile"> Click to change Photo:</label>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" id="sbm" class="btn btn-primary wd-200 pd-x-20 center-item ">Update</button>

                                <button class="btn btn-primary" id="loader" style="display: none" type="button" disabled>
                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    Saving...
                                </button>
                            </div>

                        </div>
                    </form>





                </div><!-- col -->

                <div class="col-lg-2 col-xl-2 mg-t-10">
                    &nbsp;
                </div>

            </div><!-- row -->


        </div><!-- container -->
    </div>
</div>

<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="lib/feather-icons/feather.min.js"></script>
<script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="assets/js/dashforge.js"></script>
<script src="assets/js/dashforge.aside.js"></script>
<script src="lib/parsleyjs/parsley.min.js"></script>
<script src="assets/js/alertify.js"></script>
<script src="lib/cleave.js/cleave.min.js"></script>
<script src="assets/js/dish.js"></script>



</body>


</html>
