<?php

/**
 * Created by PhpStorm.
 * User: Aleadmin
 * Date: 7/24/2017
 * Time: 5:12 PM
 */


include_once 'Config.php';

class Crud extends Config
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getData($query)
    {
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $rows = array();

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }


    public function getError()
    {
        return $this->connection->error;
    }

    public function execute($query)
    {
        $result = $this->connection->query($query);

        if ($result == false) {
            //echo 'Error: cannot execute the command';
            return false;
        } else {

            return $result;

        }
    }

    public function delete($id, $table)
    {
        $query = "DELETE FROM $table WHERE id = $id";

        $result = $this->connection->query($query);

        if ($result == false) {
            echo 'Error: cannot delete id ' . $id . ' from table ' . $table;
            return false;
        } else {
            return true;
        }
    }

    public function newdelete($id,$col,$table)
    {
        $query = "DELETE FROM $table WHERE $col = '$id'";

        $result = $this->connection->query($query);

        if ($result == false) {
            echo 'Error: cannot delete id ' . $id . ' from table ' . $table;
            return false;
        } else {
            return true;
        }
    }



    public function total_restaurant()
    {
        $query="SELECT COUNT(name) as restaurant FROM restaurant";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $res = '';

        while ($row = $result->fetch_assoc()) {
            $res = $row['restaurant'];
        }

        return $res;
    }


    public function total_dish()
    {
        $query="SELECT COUNT(dish_id) as dish FROM dish";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $res = '';

        while ($row = $result->fetch_assoc()) {
            $res = $row['dish'];
        }

        return $res;
    }


    public function total_customers()
    {
        $query="SELECT COUNT(email) as customers FROM customers";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $res = '';

        while ($row = $result->fetch_assoc()) {
            $res = $row['customers'];
        }

        return $res;
    }


    public function total_orders()
    {
        $query="SELECT COUNT(1) as orders FROM orders";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $res = '';

        while ($row = $result->fetch_assoc()) {
            $res = $row['orders'];
        }

        return $res;
    }

    public function pending_orders()
    {
        $query="SELECT COUNT(1) as orders FROM orders WHERE status = 0";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $res = '';

        while ($row = $result->fetch_assoc()) {
            $res = $row['orders'];
        }

        return $res;
    }


    public function last_20_transaction()
    {
        $query="SELECT date(o.created_at) as order_date,first_name,dish_name,qty,amount,o.status 
FROM orders o INNER JOIN customers c
ON o.cust_id = c.cust_id
INNER JOIN dish d
ON o.dish_id = d.dish_id
ORDER BY c.created_at DESC 
LIMIT 20";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        $res = [];


        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();
            $payload->o_date = $row['order_date'];
            $payload->c_name = $row['first_name'];
            $payload->d_name = $row['dish_name'];
            $payload->qty = $row['qty'];
            $payload->amount = $row['amount'];
            $payload->status = $row['status'] ==0 ? 'Pending': ($row['status']==1 ? 'Completed': 'Canceled');

            array_push($res,$payload);
        }


        return $res;
    }

    public function all_orders()
    {
        $query="SELECT date(o.created_at) as order_date,order_id,concat(first_name,' ',last_name) as fullname,mode,dish_name,qty,amount,o.status 
FROM orders o INNER JOIN customers c
ON o.cust_id = c.cust_id
INNER JOIN dish d
ON o.dish_id = d.dish_id
ORDER BY o.created_at DESC ";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        $res = [];


        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();
            $payload->o_date = $row['order_date'];
            $payload->c_name = $row['fullname'];
            $payload->d_name = $row['dish_name'];
            $payload->qty = $row['qty'];
            $payload->amount = $row['amount'];
            $payload->mode = $row['mode'];
            $payload->order_id = $row['order_id'];
            $payload->status = $row['status'] ==0 ? 'Pending': ($row['status']==1 ? 'Completed': 'Canceled');

            array_push($res,$payload);
        }


        return $res;
    }


    public function last_10_customers()
    {
        $query="SELECT first_name, last_name, date(created_at) as reg_date FROM customers ORDER BY created_at DESC LIMIT 10";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        $res = [];


        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();
            $payload->f_name = $row['first_name'];
            $payload->l_name = $row['last_name'];
            $payload->reg_date = $row['reg_date'];

            array_push($res,$payload);

        }




        return $res;
    }

    public function all_customers()
    {
        $query="SELECT cust_id,first_name, last_name, email, phone, date(created_at) as reg_date FROM customers ORDER BY created_at DESC";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        $res = [];


        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();

            $payload->cust_id = $row['cust_id'];
            $payload->f_name = $row['first_name'];
            $payload->l_name = $row['last_name'];
            $payload->email = $row['email'];
            $payload->phone = $row['phone'];
            $payload->reg_date = $row['reg_date'];

            array_push($res,$payload);

        }




        return $res;
    }


    public function all_restaurants()
    {
        $query="SELECT id,name,address,logo, date(created_at) as created_at FROM restaurant ORDER BY created_at DESC";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        $res = [];


        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();
            $payload->rest_id = $row['id'];
            $payload->name = $row['name'];
            $payload->address = $row['address'];
            $payload->logo = $row['logo'];
            $payload->created_at = $row['created_at'];

            array_push($res,$payload);

        }


        return $res;
    }

    public function all_dish()
    {
        $query="SELECT dish_id,dish_name,restaurant_name(rest_id) as rest_name,price,image, date(created_at) as created_at FROM dish ORDER BY created_at DESC";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        $res = [];


        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();
            $payload->dish_id = $row['dish_id'];
            $payload->dish_name = $row['dish_name'];
            $payload->rest_name = $row['rest_name'];
            $payload->price = $row['price'];
            $payload->image = $row['image'];
            $payload->created_at = $row['created_at'];

            array_push($res,$payload);

        }


        return $res;
    }





    public function get_restaurants_by_id($id)
    {
        $query="SELECT id,name,address,logo FROM restaurant WHERE id={$id}";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        $res = [];


        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();
            $payload->rest_id = $row['id'];
            $payload->name = $row['name'];
            $payload->address = $row['address'];
            $payload->logo = $row['logo'];

            array_push($res,$payload);

        }


        return $res;
    }

    public function get_dish_by_id($id)
    {
        $query="SELECT * FROM dish WHERE dish_id={$id}";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        $res = [];


        while ($row = $result->fetch_assoc()) {
            $payload = new stdClass();
            $payload->dish_id = $row['dish_id'];
            $payload->rest_id = $row['rest_id'];
            $payload->dish_name = $row['dish_name'];
            $payload->price = $row['price'];
            $payload->image = $row['image'];

            array_push($res,$payload);

        }


        return $res;
    }








    public function getStock($id)
    {
        $query="SELECT COUNT(stock_id) as stock FROM parts WHERE stock_id='$id';";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['stock'];
        }

        return $name;
    }

    public function getvehicleplate($id)
    {
        $query="SELECT COUNT(id) as vhc FROM vehicle WHERE plate_number='$id'";
        $result = $this->connection->query($query);

        if (mysqli_num_rows($result) <= 0) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['vhc'];
        }

        return $name;
    }

    public function totalstockleft($id)
    {
        $query="SELECT total_unit FROM warehouse WHERE stock_id='$id';";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['total_unit'];
        }

        return $name;
    }

    public function fetchpartcategory($id)
    {
        $query="SELECT stock_id,serial,cost FROM parts WHERE part_id='$id';";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }



        while ($row = $result->fetch_assoc()) {
            $stock = $row['stock_id'];
            $serial = $row['serial'];
            $cost = $row['cost'];
        }

        $name = array($stock, $serial, $cost);

        return $name;
    }

    public function totalavailable($id)
    {
        $query="SELECT total_unit FROM warehouse WHERE stock_id='$id';";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['total_unit'];
        }

        return $name;
    }

    public function getpartsleft($id)
    {
        $query="SELECT serial FROM parts WHERE part_id='$id';";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $serial = '';

        while ($row = $result->fetch_assoc()) {
            $serial = $row['serial'];
        }

        return $serial;
    }

    public function checkdata($id,$rows,$table)
    {
        $query = "SELECT $rows FROM $table WHERE id = $id";

        $result = $this->connection->query($query);

        if ($result == false) {
            echo 'Error: cannot delete id ' . $id . ' from table ' . $table;
            return false;
        } else {
            return true;
        }
    }


    public function escape_string($value)
    {
        return $this->connection->real_escape_string(trim(preg_replace('/\t+/', '',$value)));
    }

    public function sendsms($recipient,$message)
    {

        $url = "https://jusibe.com/smsapi/send_sms";
        $username = "4583587807b11a4b865f2e49a1cc00c1";
        $password = "bb3de67959fe497d291f8cea5b58f8bb";

        $data = array(
            "to" => $recipient,
            "from" => "CAAP LTD",
            "message" => $message,
        );


        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        curl_close($ch);

        $resp = json_decode($response, true);
        $msgid = $resp['message_id'];
        $stat = $resp['status'];
        $credit = $resp['sms_credits_used'];




        $this->execute("INSERT INTO smslog VALUES (NULL ,'$recipient','$message','$stat','$msgid','$credit',now())");


    }

    public static function sendmail($to,$txt,$subj)
    {


        $headers = "From: CAAP ADMIN" . "\r\n";

        mail($to,$subj,$txt,$headers);


    }

    public function getToken($length=7){
        $token = "";
        $codeAlphabet= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-3)];
        }

        return $token;
    }

    public function getdrivervehicle($did)
    {
        $name='';
        $query="SELECT vehicle_make,vehicle_model,plate_number FROM vehicle WHERE driver_id='$did'";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name .= '- '.$row['vehicle_make']." ".$row['vehicle_model']." (".$row['plate_number'].")<br> ";
        }

        return $name;
    }

    public function getworkshop($did)
    {
        $query="SELECT workshop_name FROM workshop WHERE workshop_id='$did'";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $name = '';

        while ($row = $result->fetch_assoc()) {
            $name = $row['workshop_name'];
        }

        return $name;
    }

    public function getClientDetails($cid)
    {
        $query="SELECT client_name,client_email,client_phone FROM clients WHERE client_id='$cid'";
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }


        while ($row = $result->fetch_assoc()) {
            $name = array($row['client_name'],$row['client_email'],$row['client_phone']);
        }

        return @$name;
    }


}