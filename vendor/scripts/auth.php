<?php

session_start();

include_once("Crud.php");

if(isset($_POST["username"])) {


    $admin = new Crud();

    $user_name = $admin->escape_string($_POST['username']);
    $password = sha1(md5($admin->escape_string($_POST['password'])));


    $result = $admin->execute("SELECT admin_id,username FROM login WHERE username='$user_name' && password='$password'");



    if(mysqli_num_rows($result)>0)
    {

        $_SESSION['admin_name'] = $user_name;

        echo json_encode(array("success","Authenticated"));

    }
    else{

        echo json_encode(array("error","Invalid login credentials"));
    }
}

// D A S H B O A R D - S T A T S

else if(isset($_POST["dashboard"])) {


    $admin = new Crud();

    $restaurant = $admin->total_restaurant();
    $dishes = $admin->total_dish();
    $customers = $admin->total_customers();
    $orders = $admin->total_orders();
    $last_trans = $admin->last_20_transaction();
    $recent_cust = $admin->last_10_customers();

    $_SESSION['pending_orders'] = $admin->pending_orders();


    echo json_encode(array($restaurant,$dishes,$customers,$orders,$last_trans,$recent_cust));


}


// A D D - R E S T U A R A N T


else if(isset($_POST["restaurant_name"])) {


    $admin = new Crud();

    $restaurant_name = $admin->escape_string($_POST['restaurant_name']);

    $restaurant_addr = $admin->escape_string($_POST['restaurant_addr']);

    $restaurant_lng = $admin->escape_string($_POST['restaurant_lng']);

    $restaurant_lat = $admin->escape_string($_POST['restaurant_lat']);


    if(!empty($_FILES['logo']))
    {
        $path = "../uploads/restaurants/";


        $temp = explode(".", $_FILES["logo"]["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);

        $db_name = "uploads/restaurants/".$newfilename;




            $result = $admin->execute("INSERT INTO restaurant VALUES(NULL,'$restaurant_name','$restaurant_addr','$restaurant_lng',
'$restaurant_lat','$db_name',NOW(),'Admin')");

            if($result)
            {
                move_uploaded_file($_FILES["logo"]["tmp_name"], $path . $newfilename);

                echo json_encode(array('success'));
            }

        else{
            echo json_encode(array('error','Restaurant name already exist'));
        }

    }
 else{
     echo json_encode(array('error'));
 }




}


// U P D A T E - R E S T U A R A N T


else if(isset($_POST["up_restaurant_name"])) {


    $admin = new Crud();

    $restaurant_name = $admin->escape_string($_POST['up_restaurant_name']);

    $restaurant_addr = $admin->escape_string($_POST['up_restaurant_addr']);

    $restaurant_lng = $admin->escape_string($_POST['up_restaurant_lng']);

    $restaurant_lat = $admin->escape_string($_POST['up_restaurant_lat']);

    $restaurant_id = $admin->escape_string($_POST['up_rest_id']);


    if(!empty($_FILES['logo']))
    {
        $path = "../uploads/restaurants/";


        $temp = explode(".", $_FILES["logo"]["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);

        $db_name = "uploads/restaurants/".$newfilename;



        if(move_uploaded_file($_FILES["logo"]["tmp_name"], $path . $newfilename)) {

            $result = $admin->execute("UPDATE restaurant SET name='$restaurant_name',address='$restaurant_addr',lng='$restaurant_lng',
lat='$restaurant_lat', logo='$db_name' WHERE id={$restaurant_id}");

            if($result)
            {

                echo json_encode(array('success'));
            }
            else{
                echo json_encode(array('error'));
            }



        }
        else{
            echo json_encode(array('error'));
        }

    }
    else{
        $result = $admin->execute("UPDATE restaurant SET name='$restaurant_name',address='$restaurant_addr',lng='$restaurant_lng',
lat='$restaurant_lat' WHERE id={$restaurant_id} ");

        if($result)
        {

            echo json_encode(array('success'));
        }
        else{
            echo json_encode(array('error'));
        }

    }





}



// D E L E T E - R E S T U A R A N T

else if(isset($_POST["delete_restaurant"])) {


    $admin = new Crud();
    $restaurant_id = $_POST["delete_restaurant"];

    $result = $admin->newdelete($restaurant_id,'id','restaurant');

    echo json_encode(array($result));


}



// A D D - D I S H


else if(isset($_POST["dish_name"])) {


    $admin = new Crud();

    $dish_name = $admin->escape_string($_POST['dish_name']);

    $dish_price = $admin->escape_string($_POST['dish_price']);

    $restaurant_id = $admin->escape_string($_POST['restaurant_id']);




    if(!empty($_FILES['dish_photo']))
    {
        $path = "../uploads/dish/";


        $temp = explode(".", $_FILES["dish_photo"]["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);

        $db_name = "uploads/dish/".$newfilename;





            $result = $admin->execute("INSERT INTO dish VALUES(NULL,'$restaurant_id','$dish_name','$dish_price',
'$db_name',NOW(),'Admin')");

            if($result)
            {
                move_uploaded_file($_FILES["dish_photo"]["tmp_name"], $path . $newfilename);

                echo json_encode(array('success'));
            }
            else{
                echo json_encode(array('error','Dish name already exist for same restaurant and price'));
            }




    }
    else{
        echo json_encode(array('error'));
    }




}


// U P D A T E - D I S H


else if(isset($_POST["up_dish_name"])) {


    $admin = new Crud();

    $dish_name = $admin->escape_string($_POST['up_dish_name']);

    $dish_id = $admin->escape_string($_POST['up_dish_id']);

    $dish_price = $admin->escape_string($_POST['up_dish_price']);

    $restaurant_id = $admin->escape_string($_POST['up_restaurant_id']);



    if(!empty($_FILES['up_dish_photo']))
    {
        $path = "../uploads/dish/";


        $temp = explode(".", $_FILES["up_dish_photo"]["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);

        $db_name = "uploads/dish/".$newfilename;





            $result = $admin->execute("UPDATE dish SET dish_name='$dish_name',price='$dish_price',rest_id='$restaurant_id',
              image='$db_name' WHERE dish_id={$dish_id}");

            if($result)
            {
                move_uploaded_file($_FILES["up_dish_photo"]["tmp_name"], $path . $newfilename);

                echo json_encode(array('success'));
            }
            else{
                echo json_encode(array('error', 'Dish name already exist for same restaurant and price'));
            }


    }
    else{
        $result = $admin->execute("UPDATE dish SET dish_name='$dish_name',price='$dish_price',rest_id='$restaurant_id' WHERE dish_id={$dish_id} ");

        if($result)
        {

            echo json_encode(array('success'));
        }
        else{
            echo json_encode(array('error','Dish name already exist for same restaurant and price'));
        }

    }





}



// D E L E T E - D I S H

else if(isset($_POST["delete_dish"])) {


    $admin = new Crud();
    $dish_id = $_POST["delete_dish"];

    $result = $admin->newdelete($dish_id,'dish_id','dish');

    echo json_encode(array($result));


}

// D E L E T E - C U S T O M E R

else if(isset($_POST["delete_customer"])) {


    $admin = new Crud();
    $cust_id = $_POST["delete_customer"];

    $result = $admin->newdelete($cust_id,'cust_id','customers');

    echo json_encode(array($result));


}


// D E L E T E - O R D E R

else if(isset($_POST["delete_order"])) {


    $admin = new Crud();
    $del_order = $_POST["delete_order"];

    $result = $admin->newdelete($del_order,'oder_id','orders');

    echo json_encode(array($result));


}


// C O M P L E T E - O R D E R

else if(isset($_POST["complete_order"])) {


    $admin = new Crud();
    $order_id = $_POST["complete_order"];

    $result = $admin->execute("UPDATE orders SET status=1 WHERE order_id={$order_id} ");

    $_SESSION['pending_orders'] = $admin->pending_orders();


    if($result)
    {

        echo json_encode(array('success'));
    }
    else{
        echo json_encode(array('error','An error occurred, please try again'));
    }


}


// C A N C E L - O R D E R

else if(isset($_POST["cancel_order"])) {


    $admin = new Crud();
    $order_id = $_POST["cancel_order"];

    $result = $admin->execute("UPDATE orders SET status=2 WHERE order_id={$order_id} ");

    $_SESSION['pending_orders'] = $admin->pending_orders();

    if($result)
    {

        echo json_encode(array('success'));
    }
    else{
        echo json_encode(array('error','An error occurred, please try again'));
    }


}



// C O M P R E S S - I M A G E

function compress_image($source_url, $destination_url, $quality) {

    $info = getimagesize($source_url);

    if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source_url);

    elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source_url);

    elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source_url);

    imagejpeg($image, $destination_url, $quality);

    return $destination_url;
}
