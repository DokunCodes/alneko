<?php
include_once("scripts/Crud.php");
session_start();

if(!$_SESSION['admin_name']){

    header('location: ./');
}

$pending_orders = $_SESSION['pending_orders'];

$admin = new Crud();




?>

<!DOCTYPE html>
<html lang="en">


<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Meta -->
    <meta name="description" content="BoiBoi Admin">
    <meta name="author" content="Seyi Adedokun">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <title>All Dish</title>

    <!-- vendor css -->
    <link href="lib/%40fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/jqvmap/jqvmap.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="assets/css/dashforge.css">
    <link rel="stylesheet" href="assets/css/alertify.css">
    <link href="lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="lib/select2/css/select2.min.css" rel="stylesheet">
</head>
<body>

<aside class="aside aside-fixed">
    <div class="aside-header">
        <a href="dashboard" class="aside-logo">boiboi<span>&nbsp;admin</span></a>
        <a href="#" class="aside-menu-link">
            <i data-feather="menu"></i>
            <i data-feather="x"></i>
        </a>
    </div>
    <div class="aside-body">
        <div class="aside-loggedin">
            <div class="d-flex align-items-center justify-content-start">
                <a href="#" class="avatar"><img src="assets/img/user.png" class="rounded-circle" alt=""></a>
                <div class="aside-alert-link">

                    <a href="dashboard?logout" data-toggle="tooltip" title="Sign out"><i data-feather="log-out"></i></a>
                </div>
            </div>
            <div class="aside-loggedin-user">
                <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
                    <h6 class="tx-semibold mg-b-0">Ahmad</h6>
                    <i data-feather="chevron-down"></i>
                </a>
                <p class="tx-color-03 tx-12 mg-b-0">Administrator</p>
            </div>
            <div class="collapse" id="loggedinMenu">
                <ul class="nav nav-aside mg-b-0">
                    <li class="nav-item"><a href="dashboard?logout" class="nav-link"><i data-feather="log-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
        </div><!-- aside-loggedin -->
        <ul class="nav nav-aside">
            <li class="nav-label">Dashboard</li>

            <li class="nav-item"><a href="dashboard" class="nav-link"><i data-feather="globe"></i> <span>Analytics</span></a></li>


            <li class="nav-item with-sub">
                <a href="#" class="nav-link"><i data-feather="target"></i> <span>Restaurants</span></a>
                <ul>
                    <li><a href="add-restaurant">Add New</a></li>
                    <li><a href="all-restaurant">View Restaurants</a></li>
                </ul>
            </li>
            <li class="nav-item show active with-sub">
                <a href="#" class="nav-link"><i data-feather="package"></i> <span>Dishes</span></a>
                <ul>
                    <li><a href="add-dish">Add New</a></li>
                    <li><a href="all-dish" class="active">View Dishes</a></li>
                </ul>
            </li>

            <li class="nav-item"><a href="customers" class="nav-link"><i data-feather="users"></i> <span>Customers</span></a></li>

            <?php

            if($pending_orders > 0){
                ?>
                <li class="nav-item"><a href="orders" class="nav-link"><i data-feather="shopping-cart"></i>
                        <span>Orders &nbsp;&nbsp;<span class="badge badge-pill badge-primary"><?php echo $pending_orders?></span></span></a></li>

            <?php } else{?>
                <li class="nav-item"><a href="orders" class="nav-link"><i data-feather="shopping-cart"></i> <span>Orders</span></a></li>

            <?php } ?>


        </ul>
    </div>
</aside>

<div class="content ht-100v pd-0">
    <div class="content-header">
        &nbsp;
    </div>
    <!-- content-header -->

    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                            <li class="breadcrumb-item"><a href="#">Dish</a></li>

                        </ol>
                    </nav>

                    <h4 class="mg-b-0 tx-spacing--1">All Dish</h4>


                </div>

            </div>

            <div class="row row-xs">



                <div class="col-lg-12 col-xl-12 mg-t-10">

                    <table id="example1" class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th class="wd-20p">Dish Name</th>
                            <th class="wd-10p">Price (₦)</th>
                            <th class="wd-20p">Restaurant Name</th>
                            <th class="wd-20p">Photo</th>
                            <th class="wd-20p">Date Created</th>
                            <th class="wd-20p">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $dish = $admin->all_dish();



                        for ($j=0; $j<count($dish); $j++){

                          ?>
                            <tr id="dish<?php echo $dish[$j]->dish_id  ?>">
                                <td><?php echo $j+1 ?></td>
                                <td><?php echo $dish[$j]->dish_name ?></td>
                                <td><?php echo $dish[$j]->price ?></td>
                                <td><?php echo $dish[$j]->rest_name ?></td>
                                <td><img src="<?php echo $dish[$j]->image  ?>" style=" max-height: 60px; max-width: 120px" class="img-thumbnail" /> </td>
                                <td><?php echo $dish[$j]->created_at  ?></td>
                                <td>
                                    <a href="update-dish?id=<?php echo $dish[$j]->dish_id  ?>" data-toggle="tooltip"  title="Update Dish"> <i style="color: blue" class="fas fa-pen-square"></i></a>  &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="#" onclick="deleteRestaurant('<?php echo $dish[$j]->dish_id  ?>')" data-toggle="tooltip"  title="Delete Dish"> <i style="color: red" class="fas fa-trash"></i></a>
                                </td>
                            </tr>

                        <?php
                        }

                        ?>




                        </tbody>
                    </table>



                </div><!-- col -->



            </div><!-- row -->


        </div><!-- container -->
    </div>
</div>

<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="lib/feather-icons/feather.min.js"></script>
<script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="assets/js/dashforge.js"></script>
<script src="assets/js/dashforge.aside.js"></script>
<script src="lib/parsleyjs/parsley.min.js"></script>
<script src="assets/js/alertify.js"></script>
<script src="lib/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
<script src="lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
<script src="lib/select2/js/select2.min.js"></script>
<script>
    // A L L - R E S T A U R A N T

    $('#example1').DataTable({
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    function deleteRestaurant(id) {


        alertify.confirm("Confirm Deletion?",
            function(){

                var action = 'scripts/auth.php';

                $.post(action, {
                        delete_dish: id

                    },
                    function (data) {


                        let result = $.parseJSON(data);

                        if(result[0]){
                            alertify.logPosition("top right");
                            alertify.success('Dish Deleted');
                            $('#dish'+id).remove();
                        }
                        else{
                            alertify.logPosition("top right");
                            alertify.error("An error occurred while deleting. Please try again");

                        }


                    });


            });
    }




</script>


</body>


</html>
