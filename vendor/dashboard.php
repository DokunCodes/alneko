<?php
include '../app/Crud.php';
$alneko = new Crud();
session_start();



if(isset($_GET['logout'])){
    session_destroy();
    header('location: ./');
}

if(!isset($_SESSION['vendor_id'])){
    header('location: ./');
}


$biz_name = $_SESSION['biz_name'];
$vendor_id = $_SESSION['vendor_id'];

$stats = $alneko->vendor_stats($vendor_id);


?>


<!DOCTYPE html>

<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Alneko | <?php echo $biz_name ?> </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/main-color.css" id="colors">

    <!-- Favicons-->
    <link rel="shortcut icon" href="../images/fav.png" type="image/x-icon">

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header Container
    ================================================== -->
    <header id="header-container" class="fixed fullwidth dashboard">

        <!-- Header -->
        <div id="header" class="not-sticky">
            <div class="container">

                <!-- Left Side Content -->
                <div class="left-side">

                    <!-- Logo -->
                    <div id="logo">
                        <a href="#"><img src="../images/logo-bold.png" alt=""></a>
                        <a href="#" class="dashboard-logo"><img src="../images/logo-bold.png" alt=""></a>
                    </div>

                    <!-- Mobile Navigation -->
                    <div class="mmenu-trigger">
                        <button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
                        </button>
                    </div>



                </div>
                <!-- Left Side Content / End -->

                <!-- Right Side Content / End -->
                <div class="right-side">
                    <!-- Header Widget -->
                    <div class="header-widget">

                        <!-- User Menu -->
                        <div class="user-menu">
                            <div class="user-name"><span><img src="../images/user.png" alt=""></span>My Account</div>
                            <ul>

                                <li><a href="?logout"><i class="sl sl-icon-power"></i> Logout</a></li>
                            </ul>
                        </div>

                        <a href="add-service" class="button border with-icon">Add Listing <i class="sl sl-icon-plus"></i></a>
                    </div>
                    <!-- Header Widget / End -->
                </div>
                <!-- Right Side Content / End -->

            </div>
        </div>
        <!-- Header / End -->

    </header>
    <div class="clearfix"></div>
    <!-- Header Container / End -->


    <!-- Dashboard -->
    <div id="dashboard">

        <!-- Navigation
        ================================================== -->
        <?php include 'sidemenu.php'?>

        <!-- Content
        ================================================== -->
        <div class="dashboard-content">

            <!-- Titlebar -->
            <div id="titlebar">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Welcome Back, <?php echo $_SESSION['fname'] ?>!</h2>
                        <!-- Breadcrumbs -->
                        <nav id="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li>Dashboard</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>


            <!-- Content -->
            <div class="row">

                <!-- Item -->
                <div class="col-lg-3 col-md-6">
                    <div class="dashboard-stat color-1">
                        <div class="dashboard-stat-content"><h4><?php echo $stats[0]->services ?></h4> <span>Service Listings</span></div>
                        <div class="dashboard-stat-icon"><i class="im im-icon-Map2"></i></div>
                    </div>
                </div>

                <!-- Item -->
                <div class="col-lg-3 col-md-6">
                    <div class="dashboard-stat color-2">
                        <div class="dashboard-stat-content"><h4><?php echo $stats[0]->views ?></h4> <span>Total Views</span></div>
                        <div class="dashboard-stat-icon"><i class="im im-icon-Line-Chart"></i></div>
                    </div>
                </div>


                <!-- Item -->
                <div class="col-lg-3 col-md-6">
                    <div class="dashboard-stat color-3">
                        <div class="dashboard-stat-content"><h4><?php echo $stats[0]->reviews ?></h4> <span>Total Reviews</span></div>
                        <div class="dashboard-stat-icon"><i class="im im-icon-Add-UserStar"></i></div>
                    </div>
                </div>

                <!-- Item -->
                <div class="col-lg-3 col-md-6">
                    <div class="dashboard-stat color-4">
                        <div class="dashboard-stat-content"><h4><?php echo $stats[0]->bookings ?></h4> <span>Times Booked</span></div>
                        <div class="dashboard-stat-icon"><i class="im im-icon-Heart"></i></div>
                    </div>
                </div>
            </div>


            <div class="row">


                <!-- Invoices -->
                <div class="col-lg-12 col-md-12">
                    <div class="dashboard-list-box invoices with-icons margin-top-20">
                        <h4>Recent Booking</h4>
                        <ul>

                            <?php
                            $recent_bookings = $alneko->vendor_booking($vendor_id);

                            if($recent_bookings) {

                                for ($j = 0; $j < count($recent_bookings); $j++) {
                                    $status = $recent_bookings[$j]->status ? 'paid' : 'unpaid'
                                    ?>
                                    <li><i class="list-box-icon sl sl-icon-doc"></i>
                                        <strong><?php echo $recent_bookings[$j]->user ?></strong>
                                        <ul>
                                            <li class="<?php echo $status ?>"><?php echo ucwords($status) ?> </li>
                                            <li>Booking ID: #<?php echo $recent_bookings[$j]->booking_id ?></li>
                                            <li>Date: <?php echo $recent_bookings[$j]->date ?> </li>
                                        </ul>
                                        <div class="buttons-to-right">
                                            <a href="invoice?<?php echo $recent_bookings[$j]->booking_id ?>"
                                               class="button gray">View Invoice</a>
                                        </div>
                                    </li>
                                    <?php

                                }

                            }
                            else{
                                echo '<li>No bookings yet</li>';
                            }

                            ?>


                        </ul>
                    </div>
                </div>


                <!-- Copyrights -->
                <div class="col-md-12">
                    <div class="copyrights">© 2019 Alneko. All Rights Reserved.</div>
                </div>
            </div>

        </div>
        <!-- Content / End -->


    </div>
    <!-- Dashboard / End -->


</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
<script type="text/javascript" src="../scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="../scripts/mmenu.min.js"></script>
<script type="text/javascript" src="../scripts/chosen.min.js"></script>
<script type="text/javascript" src="../scripts/slick.min.js"></script>
<script type="text/javascript" src="../scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="../scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="../scripts/waypoints.min.js"></script>
<script type="text/javascript" src="../scripts/counterup.min.js"></script>
<script type="text/javascript" src="../scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="../scripts/tooltips.min.js"></script>
<script type="text/javascript" src="../scripts/custom.js"></script>


</body>


</html>